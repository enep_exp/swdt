#!python
from ftplib import FTP
"""
Patch fixing issue when importing 'httpwatcher'

Traceback (most recent call last):
  File /home/enep/Develop/Git/swdt/src/swdt.py, line 3, in <module>
    import httpwatcher
  File /usr/lib/python3.10/site-packages/httpwatcher/__init__.py, line 4, in <module>
    from httpwatcher.server import *
  File /usr/lib/python3.10/site-packages/httpwatcher/server.py, line 13, in <module>
    import tornado.web
  File /usr/lib/python3.10/site-packages/tornado/web.py, line 86, in <module>
    from tornado import httputil
"""
import collections
try: 
    from collections import abc
    collections.MutableMapping = abc.MutableMapping
except:
    pass
import httpwatcher
import json
import sys
import argparse





config:dict = {}
msg: str = """  Tool to help web frontend developer """
a_parse = argparse.ArgumentParser(description=msg)
a_parse.add_argument("--init", type=str, nargs=3, help="Project directory initialization --init <name project> <working dir> <author>")
a_parse.add_argument("--ftp", type=str, nargs=4, help="ftp Setup --ftp <ftp host> <ftp user> <ftp passwd> <ftp remote document root>")
a_parse.add_argument("--watch", action="store_true", help="Launch live preview")
a_parse.add_argument("--upload", action="store_true", help="Full upload source in hsoting by ftp")
a_parse.add_argument("--print", action="store_true", help="Print config")


def SaveConfig():
    try:
        with open('config.json', 'w') as fp:
            fp.write(json.dumps(config, indent=4))
            fp.close()
    except IOError:
        print("Error Write config")


def InitProject(param: list):
    global config
    config['name'] = param[0]
    config['rootdir'] = param[1]
    config['author'] = param[2]
    SaveConfig()



def PrintConfig():
    global config
    print(config)


def FTPSetup(param: list):
    global config
    config['ftp']['host'] = param[0]
    config['ftp']['user'] = param[1]
    config['ftp']['passwd'] = param[2]
    config['ftp']['documentroot'] = param[3]
    SaveConfig()


def ReadConfig():
    """Function reading config"""
    global config
    try:
        with open('config.json', 'r') as fp:
            try:
                config = json.loads(fp.read())
            except json.JSONDecodeError:
                print("Error parse JSON Config")
            fp.close()
    except FileNotFoundError:
        print("The config.json file was not found. Please initialize the project with the command")
        print("   swdt --init <project name> <working dir> <author>")
        exit(-3)
    except IOError:
                print("Error Loading config")
                exit(-1)


def reloading():
    print("Server reloading")


def HServer():
    global config

if __name__ == "__main__":
    ReadConfig()
    print(msg)
    args = a_parse.parse_args()
    if args.init:
        InitProject(args.init)
    elif args.ftp:
        FTPSetup(args.ftp)
    elif args.watch:
        HServer()
    elif args.print:
        PrintConfig()
    else:
        a_parse.print_help()
    exit(0)
