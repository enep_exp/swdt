```text
███████ ██     ██ ██████  ████████
██      ██     ██ ██   ██    ██
███████ ██  █  ██ ██   ██    ██
     ██ ██ ███ ██ ██   ██    ██
███████  ███ ███  ██████     ██
```

# SWDT

## Description

Tool to help web frontend developer

## Functions

- [] - Create project directory ang gen configure file
- [] - Live preview
- [] - FTP managing

## Usage

```sh
swdt.py [-h] [--init INIT INIT INIT] [--ftp FTP FTP FTP FTP] [--watch] [--upload] [--print]
```

Options:

- ```-h, --help```            - show this help message and exit
- ```--init INIT INIT INIT``` - Project directory initialization ```--init <name project> <working dir> <author>```
- ```--ftp FTP FTP FTP FTP```   - ftp Setup ```--ftp <ftp host> <ftp user> <ftp passwd> <ftp remote document root>```
- ```--watch```                 - Launch live preview
- ```--upload```                - Full upload source in hsoting by ftp
- ```--print```                 - Print config


## Authors and acknowledgment
Show your 

## License
GPL-3

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
